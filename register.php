<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <style>
        select {
            border: 2px solid blue;
            border-radius: 0;
            box-sizing: border-box;
            background-color: white;
        }

        button {
            border: 2px solid blue;
            border-radius: 15px;
            box-sizing: border-box;
            background-color: green;
            font-size: 20px;
            color: white;
        }

        .container {
            position: fixed;
            top: 50%;
            left: 50%;
            width: 30em;
            height: auto;
            margin-top: -18em;
            margin-left: -15em;
            padding: 56px 56px;
            border: 2px solid blue;
            display: flex;
            flex-direction: column;
            gap: 15px;
            align-content: center;
        }

        .row {
            display: flex;
            justify-content: space-between;
            align-items: stretch;
            gap: 15px;
        }

        .col1 {
            display: flex;
            flex: 1;
            padding: 10px 15px;
        }

        .col2 {
            display: flex;
            flex: 3;
            justify-content: flex-start;
        }

        .label {
            background-color: green;
            border: 2px solid blue;
            color: white;
            font-size: 18px;
            justify-content: center;
        }

        .required:after {
            content: " *";
            color: red;
        }
    </style>
</head>

<body>
    <form class="container" method="post">
        <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if (empty($_POST["name"])) {
                    echo "<div class=\"row\" style=\"color: red\">Hãy nhập tên</div>";
                }
                if (!isset($_POST["gender"])) {
                    echo "<div class=\"row\" style=\"color: red\">Hãy chọn ngày sinh</div>";
                }
                if (empty($_POST["department"])) {
                    echo "<div class=\"row\" style=\"color: red\">Hãy chọn phân khoa</div>";
                }
                if (empty($_POST["birthday"])) {
                    echo "<div class=\"row\" style=\"color: red\">Hãy nhập ngày sinh</div>";
                } else {
                    $date = explode("/", $_POST["birthday"]);
                    if (!checkdate($date[1], $date[0], $date[2])) {
                        echo "<div class=\"row\" style=\"color: red\">Hãy nhập ngày sinh đúng định dạng</div>";
                    }
                }
            }
        ?>
        <div class="row">
            <div class="col1 label required">Họ và tên</div>
            <div class="col2"><input style="display: block; flex: 1; border: 2px solid blue;" type="text" name="fullname"></div>
        </div>
        <div class="row">
            <div class="col1 label required">Giới tính</div>
            <div class="col2">
                <?php
                $gender = array(
                    0 => "Nam",
                    1 => "Nữ"
                );
                for ($i = 0; $i < count($gender); $i++) {
                    echo "<div style=\"margin: auto 0;\">
                        <input style=\"accent-color: green;\" type=\"radio\" name=\"gender\" id=\"$gender[$i]\">
                            <label for=\"$gender[$i]\">$gender[$i]</label>
                        </div>
                        ";
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label required">Phân khoa</div>
            <div class="col2">
                <select style="flex: 1," name="department">
                    <?php
                    $DSKHOA = array(
                        null => "",
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học vật liệu"
                    );
                    foreach ($DSKHOA as $key => $khoa) {
                        echo "<option>$khoa</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col1 label required">Ngày sinh</div>
            <div class="col2">
                <input placeholder="dd/mm/yyyy" style="border: 2px solid blue; width: auto," name="birthday">
            </div>
        </div>
        <div class="row">
            <div class="col1 label">Địa chỉ</div>
            <div class="col2"><input style="display: block; flex: 1; border: 2px solid blue;" type="text"></div>
        </div>
        <div class="row" style="justify-content: center; margin: 25px 0 0 0;">
            <button style="padding: 16px 56px;">Đăng ký</button>
        </div>
    </form>
</body>

</html>